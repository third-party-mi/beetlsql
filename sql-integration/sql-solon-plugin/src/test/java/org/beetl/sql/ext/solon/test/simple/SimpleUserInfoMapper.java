package org.beetl.sql.ext.solon.test.simple;

import org.beetl.sql.ext.solon.test.UserInfo;
import org.beetl.sql.mapper.BaseMapper;

public interface SimpleUserInfoMapper extends BaseMapper<UserInfo> {
}
